﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(NetWebBackAdmin.Startup))]
namespace NetWebBackAdmin
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
