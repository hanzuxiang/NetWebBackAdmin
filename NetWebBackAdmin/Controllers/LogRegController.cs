﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NetWebBackAdmin.Models;
using System.Drawing;
using NetWebBackAdmin.Core.Authentication;
using NetWebBackAdmin.Core;
using NetWebBackAdmin.ViewModels;
using NetWebBackAdmin.Core.Extension;
using NetWebBackAdmin.Helper;

namespace NetWebBackAdmin.Controllers
{
    public class LogRegController : BaseController
    {
        public LogRegController()
        {}
        private readonly IFormsAuthentication _authentication;
        private readonly IRepository<Account> _accountRepository;
        private readonly IRepository<LoginLog> _loginLogRepository;
         
        public LogRegController(IFormsAuthentication authentication,
            IRepository<Account> accountRepository, IRepository<LoginLog> loginLogRepository)
        {
            _authentication = authentication;
            _accountRepository = accountRepository;
            _loginLogRepository = loginLogRepository;
        }
        [HttpPost]
        //[ValidateAntiForgeryToken]
        [AllowAnonymous]
        public ActionResult Login(LoginModel model)
        {
            if (model == null)
                throw new ArgumentNullException("model");
            //验证验证码是否正确
            var pwdErrorCount = Session["PwdErrorCount"];
            if (pwdErrorCount != null && Convert.ToInt32(pwdErrorCount) > 5)
            {
                var sessionVerifyCode = Session["VerifyCode"];
                if (sessionVerifyCode == null || model.VerifyCode != sessionVerifyCode.ToString())
                    return Json(GetResult(false, "验证码输入错误，请刷新重试。", new { errorCount = pwdErrorCount }));
            }
            var entity = _accountRepository.Table.SingleOrDefault(p => p.UserName == model.UserName);
            if (entity == null)
            {
                return Json(GetResult(false, "用户不存在。"));
            }
            if (entity.Password != model.Password.ToMd5())
            {
                int count = 1;
                var errorCount = Session["PwdErrorCount"];
                if (errorCount != null)
                    count = Convert.ToInt32(errorCount) + 1;
                Session["PwdErrorCount"] = count;

                return Json(GetResult(false, "用户名或密码输错了呢。", new { errorCount = count }));
            }
            //记录登录日志
            _loginLogRepository.Insert(new LoginLog
            {
                Ip = WebHelper.GetIp(),
                CreateTime = DateTime.Now,
                UserAgent = Request.UserAgent
            });

            //重置错误次数
            Session["PwdErrorCount"] = null;
            //保存身份票据
            _authentication.SetAuthenticationToken(entity.UserName);
            //保存登录名
            if (model.RememberMe)
            {
                HttpCookie cookie = new HttpCookie("UserName");
                cookie.Value = entity.UserName;
                cookie.Expires = DateTime.Now.AddDays(5);

                Response.Cookies.Set(cookie);
            }
            return Json(GetResult(true, "登录成功。"));
        }
	}
}